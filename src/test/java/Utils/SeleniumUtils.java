package Utils;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class SeleniumUtils {

    static String downloadPath = "src\\test\\resources\\downloaded";
    public static String screenShotPath = "src\\test\\resources\\screenshots";

    public static WebDriver getDriver(String browserType) {
        WebDriver driver = null;
        Browsers browsers = getBrowserEnumFromString(browserType);

        switch (browsers) {
            case CHROME:
                WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
                ChromeOptions chromeOptions = new ChromeOptions();
                //chromeOptions.addExtensions(new File("src\\test\\resources\\extension_9_0_5_0.crx"));
                // This is to set the dowload folder to src\test\resources\downloaded for Chrome
                Map<String, Object> preferences = new HashMap<>();
                preferences.put("download.default_directory", downloadPath);
                chromeOptions.setExperimentalOption("prefs", preferences);
                // starts without the UI of Chrome - tests will still run accordingly
                //chromeOptions.addArguments("--headless");
                // Chrome starts full screen
                chromeOptions.addArguments("--start-maximized");
                driver = new ChromeDriver(chromeOptions);
                break;
            case FIREFOX:
                WebDriverManager.getInstance(DriverManagerType.FIREFOX).setup();
                FirefoxProfile profile = new FirefoxProfile();
                //profile.addExtension(new File("src\\test\\resources\\metamask-9.0.5-an+fx.xpi"));
                // Deprecated approach - please avoid when possible
                DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
                desiredCapabilities.setCapability(FirefoxDriver.PROFILE, profile);
                //driver = new FirefoxDriver(desiredCapabilities);
                // New approach - recommended
                FirefoxOptions options = new FirefoxOptions();
                profile.setPreference("browser.download.dir", downloadPath);
                //options.setHeadless(true); // porneste firefox in modul fara interfata
                //options.addArguments("-headless"); same as setHeadless
                // how to set resolution to a certain value
                //options.addArguments("--width=1280");
                //options.addArguments("--height=1024");
                // start maximized
                options.addArguments("--start-maximized");
                options.setProfile(profile);
                driver = new FirefoxDriver(options);
                break;
            case EDGE:
                WebDriverManager.getInstance(DriverManagerType.EDGE).setup();
                driver = new EdgeDriver();
                break;
//            case OPERA:
//                WebDriverManager.getInstance(DriverManagerType.OPERA).setup();
//                driver = new OperaDriver();
//                break;
            default:
                System.out.println("WARNING selected browser is not supported: " + browsers.toString());
        }
        return driver;
    }

    public static Browsers getBrowserEnumFromString(String browserType) {
        for (Browsers browser : Browsers.values()) {
            if (browserType.equalsIgnoreCase(browser.toString()))
                return browser;
        }
        System.out.println("Browser not found on supported list");
        return null;
    }

    public static WebElement waitForGenericElement(WebDriver driver, By by, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        return wait.until(
                ExpectedConditions.presenceOfElementLocated(by)
        );
    }


}
