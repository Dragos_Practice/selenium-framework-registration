package pageObjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegistrationPage {

    private WebDriver driver;
    WebDriverWait wait;
    @FindBy(how = How.ID, using = "register-tab")
    WebElement clickRegisterTab;
    @FindBy(how = How.ID, using = "inputFirstName")
    WebElement firstNameInput;
    @FindBy(how = How.ID, using = "inputLastName")
    WebElement lastNameInput;
    @FindBy(how = How.ID, using = "inputEmail")
    WebElement emailInput;
    @FindBy(how = How.ID, using = "inputUsername")
    WebElement userNameInput;
    @FindBy(how = How.ID, using = "inputPassword")
    WebElement userPassword;
    @FindBy(how = How.ID, using = "inputPassword2")
    WebElement confirmPassword;
    @FindBy(how = How.ID, using = "register-submit")
    WebElement submitRegister;


//    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[3]/div/div[2]")
//    WebElement firstNameErr;
//    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[4]/div/div[2]")
//    WebElement lastNameErr;
//    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[5]/div[1]/div[2]")
//    WebElement emailErr;
//    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[6]/div/div[2]")
//    WebElement userNameErr;
//    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[7]/div/div[2]")
//    WebElement passWordErr;
//    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[8]/div/div[2]")
//    WebElement confirmPassWordErr;


    public RegistrationPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }


    public void register(String firstname, String lastname, String email, String username, String password, String confirmpassword) {
        firstNameInput.clear();
        firstNameInput.sendKeys(firstname);
        lastNameInput.clear();
        lastNameInput.sendKeys(lastname);
        emailInput.clear();
        emailInput.sendKeys(email);
        userNameInput.clear();
        userNameInput.sendKeys(username);
        userPassword.clear();
        userPassword.sendKeys(password);
        confirmPassword.clear();
        confirmPassword.sendKeys(confirmpassword);
        submitRegister.submit();
    }


    public void waitForRegisterPage() {
        wait.until(ExpectedConditions.elementToBeClickable(submitRegister));
    }


    public void openRegistrationPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "/stubs/auth.html");
        driver.get(hostname + "/stubs/auth.html");
        clickRegisterTab.click();

    }


}
