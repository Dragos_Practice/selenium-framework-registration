package Tests;

import Utils.OtherUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import javax.print.attribute.standard.MediaSize;
import javax.swing.*;
import java.sql.SQLOutput;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertThat;

public class myCookieHomework extends BaseTest {
    @Test
    public void cookieCheckTest() {
        driver.manage().deleteAllCookies();
        driver.get(hostname + "/stubs/cookie.html");
        OtherUtils.printCookies(driver);
        System.out.println("Creating the cookie");
        Cookie myNewCookie = new Cookie("myFirstCookie", "12345678910");
        driver.manage().addCookie(myNewCookie);
        //Cookie myDriverCookie = driver.manage().getCookieNamed("myNewCookie");
        OtherUtils.printCookies(driver);
        OtherUtils.printCookie(myNewCookie);
        Assert.assertEquals(myNewCookie.getValue(), "12345678910");
        System.out.println("Deleting the cookie");
        driver.manage().deleteCookie(myNewCookie);
        WebElement removeTheCookie = driver.findElement(By.id("delete-cookie"));
        removeTheCookie.click();
        OtherUtils.printCookies(driver);
        String newCookieValue = driver.findElement(By.id("cookie-value")).getText();
        Assert.assertEquals(newCookieValue, "");

    }

}
