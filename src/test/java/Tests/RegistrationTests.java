package Tests;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.LoginPage;
import pageObjects.ProfilePage;
import pageObjects.RegistrationPage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class RegistrationTests extends BaseTest {


    @DataProvider(name = "positiveRegisterDp")
    public Iterator<Object[]> registerDp() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        //firstName, lastName, email, userName, password, confirmPassword

        dp.add(new String[]{"dinosaur", "rex", "test@test.com", "DinoUsername", "dinosaurpassword", "dinosaurpassword"});
        dp.add(new String[]{"dingo", "tom", "test@test.com", "DingoUsername", "dingopassword", "dingopassword"});
        dp.add(new String[]{"camel", "john", "test@test.com", "CamelUsername", "camelpassword", "camelpassword"});
        dp.add(new String[]{"zebra", "mike", "test@test.com", "ZebraUsername", "zebrapassword", "zebrapassword"});

        return dp.iterator();
    }


    @Test(dataProvider = "positiveRegisterDp")
    public void registerPositive (String firstName, String lastName, String email, String username, String password, String confirmPassword) {
        RegistrationPage registrationPage = new RegistrationPage(driver);
        registrationPage.openRegistrationPage(hostname);
        registrationPage.waitForRegisterPage();
        registrationPage.register(firstName, lastName, email, username, password, confirmPassword);
        ProfilePage profilePage = new ProfilePage(driver);
        System.out.println("Registered user:" + profilePage.getUser());
        Assert.assertEquals(username, profilePage.getUser());
        profilePage.logOut();

    }



}
