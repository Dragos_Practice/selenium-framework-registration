package Tests;

import Utils.SeleniumUtils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class BaseTest {
    WebDriver driver;
    String hostname;


    @BeforeClass
    public void setUp() {
        String browserType = null;
        System.out.println(System.getProperty("browser"));
        browserType = System.getProperty("browser");   //luam browserul din linia de comanda


        try {
            //instantiere fisier properties
            InputStream input = new FileInputStream("src\\test\\java\\framework.properties"); //citire din interiorul fisierului de properties
            Properties prop = new Properties();
            prop.load(input);  //face load la stream

            //read default value from config
            if (browserType == null)
                browserType = prop.getProperty("browser");

            hostname = prop.getProperty("hostname");
            driver = SeleniumUtils.getDriver(browserType);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    @AfterClass
    public void cleanUp() {
        System.out.println("Close driver after class test");
        driver.close();
    }
}
